#!/usr/bin/python
# -*- coding: utf-8 -*-

from job_array import Job_array
from node import Node
from messenger import Messenger

from datetime import datetime
import time
from collections import deque
import os
import threading


class Grid(object):

  class Execution(object):

    def __init__(self):
      self.fail_count = 0
      self.ancestor_fail_count = 0
      self.prior_success_count = 0
      self.success_count = 0

    def all_successful(self):
      return self.fail_count == 0 and self.ancestor_fail_count == 0

  def __init__(self, script_dir, log_dir, error_log_dir):

    self.script_dir = script_dir
    self.log_dir = log_dir
    self.error_log_dir = error_log_dir

    self.roots = []
    self.nodes = []

    self.messenger = Messenger()

    self.pending_nodes = set()

    self.nthreads = 1
    self.graph_lock = threading.Lock()
    self.thread_limiter = None
    self.execution = None

    # the list of the currently running threads
    self.threads = []

    # verbosity
    self.silent_prior_success = False

  def add_root_node(self, new_job_array):
    node = Node(new_job_array)
    self.nodes.append(node)

    self.roots.append(node)
    new_job_array.messenger = self.messenger
    return node

  def after_node(self, parent_node, new_job_array):
    node = Node(new_job_array)
    self.nodes.append(node)

    parent_node.children.append(node)
    node.parents.append(parent_node)

    new_job_array.messenger = self.messenger
    return node

  def after_nodes(self, parent_nodes, new_job_array):
    node = Node(new_job_array)
    self.nodes.append(node)

    for parent_node in parent_nodes:
      parent_node.children.append(node)
      node.parents.append(parent_node)

    new_job_array.messenger = self.messenger
    return node

  def set_silent_prior_success(self, value):
    self.silent_prior_success = value

  def execute(self):
    return self.execute_impl(nthreads=1)

  def execute_multi_threaded(self, nthreads):
    return self.execute_impl(nthreads)

  def execute_impl(self, nthreads):
    self.nthreads = nthreads

    if self.nthreads > 1:
      self.thread_limiter = threading.BoundedSemaphore(self.nthreads)

    if not os.path.exists(self.script_dir):
      os.makedirs(self.script_dir)

    if not os.path.exists(self.log_dir):
      os.makedirs(self.log_dir)

    if not os.path.exists(self.error_log_dir):
      os.makedirs(self.error_log_dir)

    self.pending_nodes = set()

    # initialize states
    for node in self.nodes:
      node.nunfinished_parents = 0
      node.state = Node.State.PENDING
      self.pending_nodes.add(node)

    for node in self.nodes:
      for child in node.children:
        child.nunfinished_parents += 1

    # give script names
    taken_script_names = set()
    queue = deque()
    queue.extend(self.roots)

    while len(queue) > 0:
      node = queue.popleft()
      script_name = node.job_array.name

      i = 1
      while script_name in taken_script_names:
        script_name = '%s_%d' % (node.job_array.name, i)
        i += 1
      node.script_name = script_name
      taken_script_names.add(script_name)

      for child in node.children:
        queue.append(child)

    # start new execution
    self.execution = Grid.Execution()

    # run the root nodes
    for node in self.roots:
      assert node.state == Node.State.PENDING
      node.state = Node.State.RUNNING

    if self.nthreads > 1:
      try:
        self.graph_lock.acquire()
        for node in self.roots:
          thread = threading.Thread(target=Grid.run_node, args=(self, node))
          thread.daemon = True
          thread.start()
          self.threads.append(thread)
      finally:
        self.graph_lock.release()

      while len(self.threads) > 0:
        time.sleep(0.5)
        try:
          self.graph_lock.acquire()
          self.threads = [thread for thread in self.threads if thread.isAlive()]
        finally:
          self.graph_lock.release()
    else:
      for node in self.roots:
        self.run_node(node)

    return self.execution

  def run_node(self, node):
    if self.nthreads > 1:
      self.thread_limiter.acquire()

    try:
      if node.state == Node.State.ANCESTOR_FAIL:
        self.on_node_finished(node)
      else:
        assert node.state == Node.State.RUNNING

        node.job_array.initialize_state()

        if node.job_array.state == Job_array.State.PRIOR_SUCCESS:
          self.on_node_finished(node)
        elif node.job_array.state == Job_array.State.PENDING:
          node.state = Node.State.RUNNING
          node.job_array.execute(node.script_name, self.script_dir, self.log_dir, self.error_log_dir)
          self.on_node_finished(node)
        else:
          raise RuntimeError('Unhandled node.job_array.state: ' + str(node.job_array.state))
    finally:
      if self.nthreads > 1:
        self.thread_limiter.release()

  def on_node_finished(self, node):
    success_job_array_states = [Job_array.State.PRIOR_SUCCESS, Job_array.State.SUCCESS]
    fail_job_array_states = [Job_array.State.FAIL]

    if self.nthreads > 1:
      self.graph_lock.acquire()

    try:
      assert node.job_array.state in success_job_array_states or node.job_array.state in fail_job_array_states

      if node.state == Node.State.ANCESTOR_FAIL:
        node.job_array.print_ancestor_fail()
        self.execution.ancestor_fail_count += 1
      else:
        if node.job_array.state == Job_array.State.PRIOR_SUCCESS:
          node.state = Node.State.PRIOR_SUCCESS

          if not self.silent_prior_success:
            node.job_array.print_prior_success()

          self.execution.prior_success_count += 1
        elif node.job_array.state == Job_array.State.SUCCESS:
          node.state = Node.State.SUCCESS
          node.job_array.print_success()
          self.execution.success_count += 1
        elif node.job_array.state == Job_array.State.FAIL:
          node.job_array.print_fail()
          node.state = Node.State.FAIL
          self.execution.fail_count += 1

          # set the state of its descendants to ANCESTOR_FAIL
          stack = []
          for child in node.children:
            stack.append(child)

          while len(stack) > 0:
            descendant = stack.pop()
            descendant.state = Node.State.ANCESTOR_FAIL

            for child in descendant.children:
              stack.append(child)
        else:
          raise RuntimeError('Unhandled node.job_array.state: ' + str(node.job_array.state))

      # update the state of the related structures
      children_to_run = []
      for child in node.children:
        child.nunfinished_parents -= 1
        assert child.nunfinished_parents >= 0
        if child.nunfinished_parents == 0:
          children_to_run.append(child)

      self.pending_nodes.remove(node)

      if len(self.pending_nodes) == 0:
        self.messenger.print_title(['The execution of the work flow has finished at %s.'
                                   % datetime.now().strftime('%Y-%m-%d %H:%M')])
      else:
        # find which children to run next
        for child in children_to_run:
          assert child.state in [Node.State.PENDING, Node.State.ANCESTOR_FAIL]

          child.state = Node.State.RUNNING

        for child in children_to_run:
          if self.nthreads > 1:
            thread = threading.Thread(target=Grid.run_node, args=(self, child))
            thread.daemon = True
            thread.start()
            self.threads.append(thread)
          else:
            self.run_node(child)
    finally:
      if self.nthreads > 1:
        self.graph_lock.release()


