#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re

from grid import Grid
from job_array import Job_array
import finished_functions


def main():
  home_dir = os.getenv('HOME')
  base_dir = '%(home_dir)s/tmp' % {'home_dir': home_dir}

  # the directories where we put the bash scripts, logs etc.
  script_dir = '%(base_dir)s/grid_unittest/scripts' % {'base_dir': base_dir}
  log_dir = '%(base_dir)s/grid_unittest/logs' % {'base_dir': base_dir}
  error_log_dir = '%(base_dir)s/grid_unittest/error_logs' % {'base_dir': base_dir}

  # the location for the results
  result_dir = '%(base_dir)s/grid_unittest/results' % {'base_dir': base_dir}

  print 'script_dir: %s' % script_dir
  print 'log_dir: %s' % log_dir
  print 'error_log_dir: %s' % error_log_dir
  print 'result_dir: %s' % result_dir

  # create the directories if they don't already exist
  if not os.path.exists(script_dir):
    os.makedirs(script_dir)

  if not os.path.exists(log_dir):
    os.makedirs(log_dir)

  if not os.path.exists(error_log_dir):
    os.makedirs(error_log_dir)

  if not os.path.exists(result_dir):
    os.makedirs(result_dir)

  # Initialize SGE wrapper
  grid = Grid(script_dir, log_dir, error_log_dir)

  # We define our own finished function for the job array A.
  # The finished function expects a single integer argument, the task id here
  # denoted as mz_task_id.
  finished_function_for_a = lambda mz_task_id: os.path.exists(result_dir + '/A_' + str(mz_task_id) + '.txt')

  # Define the job array as a node in the work flow graph.
  a = grid.add_root_node(Job_array(
    memory_in_gb=2,
    time_in_minutes=55,
    ntasks=10,
    ncores=1,
    name='A',
    run_mode=Job_array.Run_mode.ON_CLUSTER,
    code="""#!/bin/bash
echo 'hi from A!'
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/A_${MZ_TASK_ID}.txt"
touch $result_path
sleep 3;
echo 'goodbye from A!'
"""
      % {'result_dir': result_dir},
    finished_function=finished_function_for_a,
    nretries=2,
    ))

  # Job array B
  #
  # We use provided finished function from the finished_functions module. It
  # expects a template string that will be supplied a dictionary with the
  # corresponding task id given as "mz_task_id" key.
  #
  # Additionally, only two tasks of the job array B should run in parallel at
  # the time, which we specify by providing max_running_tasks argument.
  b = grid.after_node(a, Job_array(
    memory_in_gb=2,
    time_in_minutes=55,
    ntasks=20,
    ncores=1,
    max_running_tasks=2,
    name='B',
    run_mode=Job_array.Run_mode.ON_CLUSTER,
    code="""#!/bin/bash
echo 'hi from B!'
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/B_${MZ_TASK_ID}.txt"
touch $result_path
sleep 20;
echo 'goodbye from B!'
"""
      % {'result_dir': result_dir},
    finished_function=finished_functions.Path_exists('%(result_dir)s/B_%%(mz_task_id)d.txt'
                                                     % {'result_dir': result_dir}),
    nretries=2,
    ))

  # Job array C
  #
  # Note how we instructed the grid wrapper to run the task locally by
  # supplying the respective run_mode argument.
  c = grid.after_node(a, Job_array(
    memory_in_gb=2,
    time_in_minutes=55,
    ntasks=3,
    ncores=1,
    name='C',
    run_mode=Job_array.Run_mode.LOCALLY,
    code="""#!/bin/bash
echo 'hi from C!'
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/C_${MZ_TASK_ID}.txt"
touch $result_path
sleep 3;
echo 'goodbye from C!'
"""
      % {'result_dir': result_dir},
    finished_function=finished_functions.Path_exists('%(result_dir)s/C_%%(mz_task_id)d.txt'
                                                     % {'result_dir': result_dir}),
    nretries=2,
    ))

  # Job array D
  #
  # Mind how we indicate which job arrays should complete before the job array
  # D can be run.
  d = grid.after_nodes([b, c], Job_array(
    memory_in_gb=2,
    time_in_minutes=55,
    ntasks=1,
    ncores=1,
    name='D',
    run_mode=Job_array.Run_mode.ON_CLUSTER,
    code="""#!/bin/bash
echo 'hi from D!'
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/D_${MZ_TASK_ID}.txt"
touch $result_path
sleep 3;
echo 'goodbye from D!'
"""
      % {'result_dir': result_dir},
    finished_function=finished_functions.Path_exists('%(result_dir)s/D_%%(mz_task_id)d.txt'
                                                     % {'result_dir': result_dir}),
    nretries=2,
    ))

  # Execute the work flow
  grid.execute()


if __name__ == '__main__':
  main()
