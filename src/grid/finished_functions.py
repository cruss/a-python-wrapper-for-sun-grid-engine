#!/usr/bin/python
# -*- coding: utf-8 -*-

import os


class Path_exists(object):

  def __init__(self, path_tpl):
    self.path_tpl = path_tpl

  def __call__(self, mz_task_id):
    path = self.path_tpl % {'mz_task_id': mz_task_id}
    return os.path.exists(path)


