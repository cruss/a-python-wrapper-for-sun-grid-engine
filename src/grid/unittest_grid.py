#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re
import shutil

from grid import Grid
from job_array import Job_array
import finished_functions


def main():
  script_dir = '/home/mristin/tmp/grid_unittest/scripts'
  log_dir = '/home/mristin/tmp/grid_unittest/logs'
  error_log_dir = '/home/mristin/tmp/grid_unittest/error_logs'
  result_dir = '/home/mristin/tmp/grid_unittest/results'

  print 'script_dir: %s' % script_dir
  print 'log_dir: %s' % log_dir
  print 'error_log_dir: %s' % error_log_dir
  print 'result_dir: %s' % result_dir
  print


  code_tpl = \
"""
echo 'hi from %%(name)s!';
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/%%(name)s_${MZ_TASK_ID}.txt"
tmp_path="%(result_dir)s/%%(name)s_oi_${MZ_TASK_ID}.txt"

if(($MZ_TASK_ID==1)); then 
  touch $result_path
  echo "Creating $result_path"
else
  if [ ! -f $tmp_path ]; then
    echo "oi" > $tmp_path
    exit 0
  else
    touch $result_path
    echo "Created $result_path"
  fi
fi

sleep 3; 
""" \
    % {'result_dir': result_dir}

  run_modes=[Job_array.Run_mode.ON_CLUSTER, Job_array.Run_mode.LOCALLY]
  nthreads_list=[1, 2]

  for run_mode in run_modes:
    if run_mode==Job_array.Run_mode.ON_CLUSTER:
      run_mode_str="ON_CLUSTER"
    elif run_mode==Job_array.Run_mode.LOCALLY:
      run_mode_str="LOCALLY"
    else:
      raise RuntimeError('Unhandled run_mode: ' + str(run_mode));

    for nthreads in nthreads_list:
      print "* * * * * * * * * * * * * * * * * * * * * * * * " 
      print "%% run_mode: %s, nthreads: %d" %(run_mode_str,nthreads)
      print ". . . . . .. . .. . .. . .. . .. . .. . .. . .. . . "    

      dirs_to_be_reset=[script_dir,log_dir,error_log_dir,result_dir]
      for reset_dir in dirs_to_be_reset:
        if os.path.exists(reset_dir):
          shutil.rmtree(reset_dir)
        os.makedirs(reset_dir)
      
      grid = Grid(script_dir, log_dir, error_log_dir)

      name = 'A'
      a = grid.add_root_node(Job_array(
        memory_in_gb=2,
        time_in_minutes=55,
        ntasks=3,
        ncores=1,
        name=name,
        run_mode=run_mode,
        code=code_tpl % {'name': name},
        finished_function=finished_functions.Path_exists('%(result_dir)s/%(name)s_%%(mz_task_id)d.txt'
                                                         % {'result_dir': result_dir, 'name': name}),
        nretries=2,
        ))

      name = 'B'
      b = grid.add_root_node(Job_array(
        memory_in_gb=2,
        time_in_minutes=55,
        ntasks=2,
        ncores=1,
        name=name,
        run_mode=run_mode,
        code=code_tpl % {'name': name},
        finished_function=finished_functions.Path_exists('%(result_dir)s/%(name)s_%%(mz_task_id)d.txt'
                                                         % {'result_dir': result_dir, 'name': name}),
        nretries=2,
        ))

      name = 'C'
      c = grid.add_root_node(Job_array(
        memory_in_gb=2,
        time_in_minutes=55,
        ntasks=2,
        ncores=1,
        name=name,
        run_mode=run_mode,
        code=code_tpl % {'name': name},
        finished_function=finished_functions.Path_exists('%(result_dir)s/%(name)s_%%(mz_task_id)d.txt'
                                                         % {'result_dir': result_dir, 'name': name}),
        nretries=2,
        ))

      name = 'D'
      d = grid.after_node(a, Job_array(
        memory_in_gb=2,
        time_in_minutes=55,
        ntasks=1,
        ncores=1,
        name=name,
        run_mode=run_mode,
        code=code_tpl % {'name': name},
        finished_function=finished_functions.Path_exists('%(result_dir)s/%(name)s_%%(mz_task_id)d.txt'
                                                         % {'result_dir': result_dir, 'name': name}),
        nretries=2,
        ))

      name = 'E'
      e = grid.after_node(a, Job_array(
        memory_in_gb=2,
        time_in_minutes=55,
        ntasks=1,
        ncores=1,
        name=name,
        run_mode=run_mode,
        code=code_tpl % {'name': name},
        finished_function=finished_functions.Path_exists('%(result_dir)s/%(name)s_%%(mz_task_id)d.txt'
                                                         % {'result_dir': result_dir, 'name': name}),
        nretries=2,
        ))

      name = 'F'
      f = grid.after_nodes([b, d], Job_array(
        memory_in_gb=2,
        time_in_minutes=55,
        ntasks=1,
        ncores=1,
        name=name,
        run_mode=run_mode,
        code=code_tpl % {'name': name},
        finished_function=finished_functions.Path_exists('%(result_dir)s/%(name)s_%%(mz_task_id)d.txt'
                                                         % {'result_dir': result_dir, 'name': name}),
        nretries=2,
        ))

      name = 'G'
      g = grid.after_nodes([e, f], Job_array(
        memory_in_gb=2,
        time_in_minutes=55,
        ntasks=1,
        ncores=1,
        name=name,
        run_mode=run_mode,
        code=code_tpl % {'name': name},
        finished_function=finished_functions.Path_exists('%(result_dir)s/%(name)s_%%(mz_task_id)d.txt'
                                                         % {'result_dir': result_dir, 'name': name}),
        nretries=2,
        ))

      # H will never finish
      name = 'H'
      h = grid.after_node(g, Job_array(
        memory_in_gb=2,
        time_in_minutes=55,
        ntasks=1,
        ncores=1,
        name=name,
        run_mode=run_mode,
        code="",
        finished_function=lambda x: False,
        nretries=1,
        ))

      grid.execute_multi_threaded(nthreads)


if __name__ == '__main__':
  main()
