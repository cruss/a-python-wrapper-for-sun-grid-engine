#!/usr/bin/python
# -*- coding: utf-8 -*-


class Node(object):

  class State(object):

    UNKNOWN = 0
    PRIOR_SUCCESS = 1
    PENDING = 2
    RUNNING = 3
    ANCESTOR_FAIL = 4
    FAIL = 5
    SUCCESS = 6

  def __init__(self, job_array):
    self.job_array = job_array
    self.script_name=None
    self.parents = []
    self.children = []
    self.root_distance = 0
    self.state = Node.State.UNKNOWN
    self.nunfinished_parents = -1

  def __repr__(self):
    return 'Node(%s)' % repr(self.job_array.name)


