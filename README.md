# README #

### Overview ###

This piece of software is a wrapper around the bash commands of the Sun Grid Engine (SGE). It makes it easier for you to design a work flow and execute it, either locally on your machine or on the SGE cluster. 

You have to give the code of your job arrays as a plain bash script. A special variable, MZ_TASK_ID, running from 0 to ntasks - 1, will be supplied as a parameter so that you can identify in your code which task of the job array is executed. 

In order for the wrapper to know which tasks finished (and which ones failed), you provide a python function. Once all the tasks have finished, the script checks which job arrays completely finished and proceeds to execute the next job array in the graph. If a job array failed to complete after a pre-defined number of retries, the error message is reported and the wrapper will not run its descendants in the work flow graph. 

### Installation ###

Clone the repository somewhere on your system and add *src* directory to your PYTHONPATH. That's all.

### Example usage ###

Let us illustrate how to use the wrapper on the small example. We will have job arrays A (10 tasks), B (20 tasks), C (3 tasks) and D (1 task). Each one of them will just create a single file in the *~/tmp* directory which we will call here *result file* for convenience. The file name will be the name of the job array concatenated with the task number (we start indexing from 0). For example, the second task of the job array B will create the result file *~/tmp/B_1.txt*. The job array A should run first. Once it has finished, the job arrays B and C should be run. Once both of them finished, the job array D should finally run. The job arrays A, B and D should run on cluster, while the job array C should run locally. Additionally, only two tasks of the job array B should run in parallel at the same time (e.g., imagine there were a high I/O load. We don't want to run many tasks running in parallel at the same time, since that could lead to a congestion). 

To know whether a task of a job array has successfully finished or not, we check whether its result file exists. The module *grid.finished_functions* provides us with some pre-defined functions which we can readily use. 

The following program will accomplish the above-mentioned work flow:

```
#!python

#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re

from grid.grid import Grid
from grid.job_array import Job_array
from grid import finished_functions

def main():
  home_dir = os.getenv('HOME')
  base_dir = '%(home_dir)s/tmp' % {'home_dir': home_dir}

  # the directories where we put the bash scripts, logs etc.
  script_dir = '%(base_dir)s/grid_unittest/scripts' % {'base_dir': base_dir}
  log_dir = '%(base_dir)s/grid_unittest/logs' % {'base_dir': base_dir}
  error_log_dir = '%(base_dir)s/grid_unittest/error_logs' % {'base_dir': base_dir}

  # the location for the results
  result_dir = '%(base_dir)s/grid_unittest/results' % {'base_dir': base_dir}

  print 'script_dir: %s' % script_dir
  print 'log_dir: %s' % log_dir
  print 'error_log_dir: %s' % error_log_dir
  print 'result_dir: %s' % result_dir

  # create the directories if they don't already exist
  if not os.path.exists(script_dir):
    os.makedirs(script_dir)

  if not os.path.exists(log_dir):
    os.makedirs(log_dir)

  if not os.path.exists(error_log_dir):
    os.makedirs(error_log_dir)

  if not os.path.exists(result_dir):
    os.makedirs(result_dir)

  # Initialize SGE wrapper
  grid = Grid(script_dir, log_dir, error_log_dir)

  # We define our own finished function for the job array A.
  #
  # The finished function expects a single integer argument, the task id, here
  # denoted as mz_task_id.
  finished_function_for_a = lambda mz_task_id: os.path.exists(result_dir + '/A_' + str(mz_task_id) + '.txt')

  # The function grid.add_root_node allows us to define a root node in the work flow graph. 
  # In this case, we specify our job array A.
  a = grid.add_root_node(Job_array(
    memory_in_gb=2,
    time_in_minutes=55,
    ntasks=10,
    ncores=1,
    name='A',
    run_mode=Job_array.Run_mode.ON_CLUSTER,
    code="""#!/bin/bash
echo 'hi from A!'
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/A_${MZ_TASK_ID}.txt"
touch $result_path
sleep 3;
echo 'goodbye from A!'
"""
      % {'result_dir': result_dir},
    finished_function=finished_function_for_a,
    nretries=2,
    ))

  # Job array B
  #
  # Since we want the job array B to run _after_ job array A, we use the 
  # function grid.after_node.
  #
  # We use provided finished function from the finished_functions module. It
  # expects a template string that will be supplied a dictionary with the
  # corresponding task id given as "mz_task_id" key.
  #
  # Additionally, only two tasks of the job array B should run in parallel at
  # the time, which we specify by providing max_running_tasks argument.
  b = grid.after_node(a, Job_array(
    memory_in_gb=2,
    time_in_minutes=55,
    ntasks=20,
    ncores=1,
    max_running_tasks=2,
    name='B',
    run_mode=Job_array.Run_mode.ON_CLUSTER,
    code="""#!/bin/bash
echo 'hi from B!'
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/B_${MZ_TASK_ID}.txt"
touch $result_path
sleep 3;
echo 'goodbye from B!'
"""
      % {'result_dir': result_dir},
    finished_function=finished_functions.Path_exists('%(result_dir)s/B_%%(mz_task_id)d.txt'
                                                     % {'result_dir': result_dir}),
    nretries=2,
    ))

  # Job array C
  #
  # Note how we instructed the grid wrapper to run the task locally by
  # supplying the respective run_mode argument.
  c = grid.after_node(a, Job_array(
    memory_in_gb=2,
    time_in_minutes=55,
    ntasks=3,
    ncores=1,
    name='C',
    run_mode=Job_array.Run_mode.LOCALLY,
    code="""#!/bin/bash
echo 'hi from C!'
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/C_${MZ_TASK_ID}.txt"
touch $result_path
sleep 3;
echo 'goodbye from C!'
"""
      % {'result_dir': result_dir},
    finished_function=finished_functions.Path_exists('%(result_dir)s/C_%%(mz_task_id)d.txt'
                                                     % {'result_dir': result_dir}),
    nretries=2,
    ))

  # Job array D
  #
  # Since the job array D should run after both job arrays B and C have finished,
  # we call the function grid.after_nodes which allows us to define multiple
  # predecessor nodes in the work flow graph.
  d = grid.after_nodes([b, c], Job_array(
    memory_in_gb=2,
    time_in_minutes=55,
    ntasks=1,
    ncores=1,
    name='D',
    run_mode=Job_array.Run_mode.ON_CLUSTER,
    code="""#!/bin/bash
echo 'hi from D!'
echo "MZ_TASK_ID: ${MZ_TASK_ID}"
result_path="%(result_dir)s/D_${MZ_TASK_ID}.txt"
touch $result_path
sleep 3;
echo 'goodbye from D!'
"""
      % {'result_dir': result_dir},
    finished_function=finished_functions.Path_exists('%(result_dir)s/D_%%(mz_task_id)d.txt'
                                                     % {'result_dir': result_dir}),
    nretries=2,
    ))

  # Execute the work flow
  grid.execute()


if __name__ == '__main__':
  main()
```

### Multi-threading ###

Most of the times you can design your work flow such that a single job array already poses a load heavy enough for your cluster to struggle. But in certain scenarios, individual job arrays can be very "light" and running them one by one could cause our resources to be under-utilized. To avoid that, the wrapper can employ multiple threads in order to keep submitting new job arrays as soon as all their parents has been successfully executed. 

It is very easy to introduce multi-threaded executation. For example, suppose we want to execute job arrays in parallel, in ten threads at the time. We just need to change:

```
#!python
grid.execute()
```

to:
```
#!python
grid.execute_multi_threaded(nthreads=10)
```

and that's it.

### Licence ###

GPLv3

GPLv3: http://gplv3.fsf.org/

All programs in this collection are free software: 
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Copyright 2014 Marko Ristin